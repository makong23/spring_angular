package com.example.spring_angular.controller.api;

import com.example.spring_angular.dto.ClientDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.example.spring_angular.utils.Constants.APP_ROOT;

public interface ClientApi {

    @PostMapping(value = APP_ROOT +"/clients/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ClientDto save(@RequestBody ClientDto dto);

    @GetMapping(value = APP_ROOT +"/clients/{IdClient}", produces = MediaType.APPLICATION_JSON_VALUE)
    ClientDto findById(@PathVariable("IdClient") Integer id);

    @GetMapping(value = APP_ROOT +"/clients/all", produces = MediaType.APPLICATION_JSON_VALUE)
    List<ClientDto> findAll();

    @DeleteMapping(value = APP_ROOT + "/clients/delete/{IdClient}")
    void delete(@PathVariable("IdClient") Integer id);
}
