package com.example.spring_angular.dto;


import com.example.spring_angular.model.Article;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class ArticleDto {

    private Integer id;

    private String codeArticle;

    private String designation;

    private BigDecimal prixUnitaireHt;

    private BigDecimal tauxTva;

    private BigDecimal prixUnitairettc;

    private String photo;

    @JsonIgnore
    private CategoryDto category;

    public static ArticleDto fromEntity(Article article){
        if(article == null){
            return null;
        }

        return ArticleDto.builder()
                .id(article.getId())
                .codeArticle(article.getCodeArticle())
                .designation(article.getDesignation())
                .prixUnitaireHt(article.getPrixUnitaireHt())
                .tauxTva(article.getTauxTva())
                .prixUnitairettc(article.getPrixUnitairettc())
                .photo(article.getPhoto())
                .build();
    }

    public static Article toEntity(ArticleDto articleDto){
        if(articleDto == null){
            return null;
        }

        Article article = new Article();
        article.setId(article.getId());
        article.setCodeArticle(article.getCodeArticle());
        article.setDesignation(article.getDesignation());
        article.setPrixUnitaireHt(article.getPrixUnitaireHt());
        article.setTauxTva(article.getTauxTva());
        article.setPrixUnitairettc(article.getPrixUnitairettc());
        article.setPhoto(article.getPhoto());

        return article;
    }
}
