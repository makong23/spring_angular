package com.example.spring_angular.dto;

import com.example.spring_angular.model.Entreprise;
import com.example.spring_angular.model.LigneCommandeClient;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class LigneCommandeClientDto {

    private Integer id;

    @JsonIgnore
    private ArticleDto article;

    @JsonIgnore
    private CommandeClientDto commandeclient;

    public static LigneCommandeClientDto fromEntity(LigneCommandeClient ligneCommandeClient){
        if(ligneCommandeClient == null){
            return null;
        }

        return LigneCommandeClientDto.builder()
                .id(ligneCommandeClient.getId())
                .build();
    }

    public static LigneCommandeClient toEntity(LigneCommandeClientDto ligneCommandeClientDto){
        if(ligneCommandeClientDto == null){
            return null;
        }

        LigneCommandeClient ligneCommandeClient = new LigneCommandeClient();
        ligneCommandeClient.setId(ligneCommandeClientDto.getId());

        return ligneCommandeClient;
    }
}
