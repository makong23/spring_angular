package com.example.spring_angular.dto;

import com.example.spring_angular.model.Entreprise;
import com.example.spring_angular.model.Role;
import com.example.spring_angular.model.Utilisateur;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;


@Data
@Builder
public class RoleDto {

    private Integer id;

    private  String roleName;

    @JsonIgnore
    private UtilisateurDto utilisateur;

    public static RoleDto fromEntity(Role role){
        if(role == null){
            return null;
        }

        return RoleDto.builder()
                .id(role.getId())
                .roleName(role.getRoleName())
                .build();
    }

    public static Role toEntity(RoleDto roleDto){
        if(roleDto == null){
            return null;
        }

        Role role = new Role();
        role.setId(roleDto.getId());
        role.setRoleName(roleDto.getRoleName());

        return role;
    }
}
