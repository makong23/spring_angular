package com.example.spring_angular.exception;

public enum ErrorCodes {
    ARTICLE_NOT_FOUND(1000),
    ARTICLE_NOT_VALID(1001),
    CATEGORY_NOT_FOUND(2000),
    CATEGORY_NOT_VALID(2001),
    CLIENT_NOT_FOUND(3000),
    CLIENT_NOT_VALID(3001),
    COMMANDE_CLIENT_NOT_FOUND(4000),
    COMMANDE_CLIENT_NOT_VALID(4001),
    ENTREPRISE_NOT_FOUND(5000),
    ENTREPRISE_NOT_VALID(5001),
    LINGE_COMMANDE_CLIENT_NOT_FOUND(6000),
    LINGE_COMMANDE_CLIENT_NOT_VALID(6001),
    ROLE_NOT_FOUND(7000),
    ROLE_NOT_VALID(7001),
    UTILISATEUR_NOT_FOUND(8000),
    UTILISATEUR_NOT_VALID(8001),
    ;

    private int code;

    ErrorCodes(int code){
        this.code = code;
    }

    public int getCode(){
        return code;
    }
}
