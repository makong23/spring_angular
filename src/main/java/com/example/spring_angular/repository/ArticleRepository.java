package com.example.spring_angular.repository;

import com.example.spring_angular.model.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ArticleRepository extends JpaRepository<Article, Integer> {
    Optional<Article> findArticleByCodeArticle(String codeArticle);

    Optional<Article> findByid(Integer id);
}
