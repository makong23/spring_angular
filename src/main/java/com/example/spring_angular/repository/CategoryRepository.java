package com.example.spring_angular.repository;

import com.example.spring_angular.model.Article;
import com.example.spring_angular.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Optional<Category> findCategoryByCode(String code);
}
