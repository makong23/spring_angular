package com.example.spring_angular.repository;

import com.example.spring_angular.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Integer> {
}
