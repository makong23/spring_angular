package com.example.spring_angular.services;

import com.example.spring_angular.dto.ArticleDto;
import com.example.spring_angular.dto.CategoryDto;

import java.util.List;

public interface CategoryService {

    CategoryDto save(CategoryDto dto);

    CategoryDto findById(Integer id);

    CategoryDto findByCode(String code);

    List<CategoryDto> findAll();

    void delete(Integer id);

}
