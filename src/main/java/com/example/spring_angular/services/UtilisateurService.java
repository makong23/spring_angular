package com.example.spring_angular.services;

import com.example.spring_angular.dto.EntrepriseDto;
import com.example.spring_angular.dto.UtilisateurDto;

import java.util.List;

public interface UtilisateurService {

    UtilisateurDto save(UtilisateurDto dto);

    UtilisateurDto findById(Integer id);

    List<UtilisateurDto> findAll();

    void delete(Integer id);

}
