package com.example.spring_angular.services.impl;

import com.example.spring_angular.dto.CommandeClientDto;
import com.example.spring_angular.exception.EntityNotFoundException;
import com.example.spring_angular.exception.ErrorCodes;
import com.example.spring_angular.exception.InvalidEntityException;
import com.example.spring_angular.model.Article;
import com.example.spring_angular.model.Client;
import com.example.spring_angular.model.CommandeClient;
import com.example.spring_angular.repository.ArticleRepository;
import com.example.spring_angular.repository.ClientRepository;
import com.example.spring_angular.repository.CommandeClientRepository;
import com.example.spring_angular.services.CommandeClientService;
import com.example.spring_angular.validator.CommandeClientValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CommandeServiceImpl implements CommandeClientService {

    private CommandeClientRepository commandeClientRepository;
    private ClientRepository clientRepository;
    private ArticleRepository articleRepository;

    public CommandeServiceImpl(
            CommandeClientRepository commandeClientRepository,
            ClientRepository clientRepository,
            ArticleRepository articleRepository
    ) {
        this.commandeClientRepository = commandeClientRepository;
        this.clientRepository = clientRepository;
        this.articleRepository = articleRepository;
    }

    @Override
    public CommandeClientDto save(CommandeClientDto dto) {

        List<String> errors = CommandeClientValidator.validate(dto);
        if(!errors.isEmpty()){
            log.error("Commande client is not valid {}", dto);
            throw new InvalidEntityException("Commande client n'est pas valide", ErrorCodes.COMMANDE_CLIENT_NOT_VALID, errors);
        }

        Optional<Client> client = clientRepository.findById(dto.getClient().getId());
        if(!client.isPresent()){
            log.warn("client with ID {} was not found id the bd", dto.getClient().getId());
            throw new EntityNotFoundException("Aucun client avec l'ID" + dto.getClient().getId()+
                    "n'a ete trouve dans BDD");
        }

        List<String> articleErrors = new ArrayList<>();

        if(dto.getLigneCommandeClients() != null){
            dto.getLigneCommandeClients().forEach(ligCmdClt -> {
                Optional<Article> article = articleRepository.findByid(ligCmdClt.getArticle().getId());
                if (article.isEmpty()){
                    articleErrors.add("L'artice avec l'ID"+ ligCmdClt.getArticle().getId() +"n'existe pas");
                }
            });
        }

        if(!articleErrors.isEmpty()){
            log.warn("");
            throw new InvalidEntityException("Article n'est pas dans le BDD", ErrorCodes.ARTICLE_NOT_VALID, articleErrors);
        }

        return CommandeClientDto.fromEntity(
                commandeClientRepository.save(CommandeClientDto.toEntity(dto))
        );
    }

    @Override
    public CommandeClientDto findById(Integer id) {
        if(id == null){
            log.error("Commande Client ID is null");
            return null;
        }

        Optional<CommandeClient> commandeClient = commandeClientRepository.findById(id);
        return Optional.of(CommandeClientDto.fromEntity(commandeClient.get())).orElseThrow(() ->
                new EntityNotFoundException(
                        "Aucune Commande Client avec l'ID ="+ id +" n'ete trouve dans le BDD",
                        ErrorCodes.COMMANDE_CLIENT_NOT_FOUND)
        );
    }

    @Override
    public CommandeClientDto findByCode(String code) {

        if(!StringUtils.hasLength(code) ){
            log.error("Commande Client CODE is null");
            return null;
        }

        Optional<CommandeClient> commandeClient = commandeClientRepository.findCommandeByCode(code);

        return Optional.of(CommandeClientDto.fromEntity(commandeClient.get())).orElseThrow(() ->
                new EntityNotFoundException(
                        "Aucune Commande Client avec le CODE ="+ code +" n' ete trouve dans le BDD",
                        ErrorCodes.COMMANDE_CLIENT_NOT_FOUND)
        );
    }

    @Override
    public List<CommandeClientDto> findAll() {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }
}
