package com.example.spring_angular.validator;

import com.example.spring_angular.dto.ArticleDto;
import com.example.spring_angular.dto.UtilisateurDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class ArticleValidator {

    public static List<String> validate(ArticleDto articleDto){
        List<String> errors = new ArrayList<>();

        if (articleDto == null){
            errors.add("Veuillez renseigner le code d'article");
            errors.add("Veuillez renseigner le designation d'article");
            errors.add("Veuillez renseigner le prix unitaire HT");
            errors.add("Veuillez renseigner le taux TVA");
            errors.add("Veuillez renseigner le prix unitaire TTC ");
            return errors;
        }
        if (!StringUtils.hasLength(articleDto.getCodeArticle())){
            errors.add("Veuillez renseigner le code d'article");
        }
        if (!StringUtils.hasLength(articleDto.getDesignation())){
            errors.add("Veuillez renseigner le designation d'article");
        }
        if (articleDto.getPrixUnitaireHt() == null){
            errors.add("Veuillez renseigner le prix unitaire HT");
        }
        if (articleDto.getTauxTva() == null){
            errors.add("Veuillez renseigner le taux TVA");
        }
        if (articleDto.getPrixUnitairettc() == null){
            errors.add("Veuillez renseigner le prix unitaire TTC ");
        }
        return errors;
    }

}
