package com.example.spring_angular.validator;

import com.example.spring_angular.dto.CommandeClientDto;
import com.example.spring_angular.dto.RoleDto;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class RoleValidator {
    public static List<String> validate(RoleDto roleDto){
        List<String> errors = new ArrayList<>();
        
        if (!StringUtils.hasLength(roleDto.getRoleName())){
            errors.add("Veuillez renseigner le nom du role ");
        }
        return errors;
    }
}
